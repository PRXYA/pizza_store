/*
Assignment - Homework 02
File - MainActivity.java
Name - Priya Patel & Pallav Jhaveri
 */

package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebIconDatabase;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public String[] items = {"Bacon", "Cheese", "Garlic", "Green Pepper", "Mushroom", "Olives","Onions", "Red Pepper"};
    static public String[] selections = new String[10];
    public int counter = 0;

    double basePrize = 6.50;
    double toppingPrize=0;

    Boolean delivery;

    Button btnCalculateTotal;
    CheckBox deliveryCheckBox;
    TypedArray img;
    ProgressBar pgbar;
    ImageView iv_item1;
    ImageView iv_item2;
    ImageView iv_item3;
    ImageView iv_item4;
    ImageView iv_item5;
    ImageView iv_item6;
    ImageView iv_item7;
    ImageView iv_item8;
    ImageView iv_item9;
    ImageView iv_item10;

    ImageView[] iv = new ImageView[10];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ConstraintLayout constLayout = (ConstraintLayout) findViewById(R.id.constraint1);
        btnCalculateTotal = findViewById(R.id.btnCheckout);
        deliveryCheckBox=findViewById(R.id.checkBox);

        //Removes individual toppings when clicked
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = (Integer) v.getTag();
                selections[index] = null;
                String[] temp = new String[10];
                int temp_counter = 0;

                for(int i = 0; i < 10; i++){
                    if(selections[i] != null){
                        temp[temp_counter] = selections[i];
                        temp_counter++;
                    } else {
                        temp[temp_counter] = null;
                    }
                }

                //updates toppings selections
                selections = temp;
                counter = temp_counter;

                //updates progress bar
                int progress = pgbar.getProgress();
                pgbar.setProgress(progress-10);

                //re-assigns image views
                setImages();
            }

            private void setImages() {
                for(int i = 0; i <= counter; i++){
                    for(int j = 0; j < items.length; j++){
                        if(selections[i] != null){
                            if(items[j] == selections[i]){
                                iv[i].setImageResource(img.getResourceId(j, -1));
                            }
                        } else {
                            iv[i].setImageDrawable(null);
                        }

                    }
                }
            }
        };


        iv_item1 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[0] = iv_item1;

        iv_item2 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[1] = iv_item2;

        iv_item3 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[2] = iv_item3;

        iv_item4 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[3] = iv_item4;

        iv_item5 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[4] = iv_item5;

        iv_item6 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[5] = iv_item6;

        iv_item7 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[6] = iv_item7;

        iv_item8 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[7] = iv_item8;

        iv_item9 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[8] = iv_item9;

        iv_item10 = new ImageView(com.example.myapplication.MainActivity.this);
        iv[9] = iv_item10;

        //access array of image paths from array.xml
        img = getResources().obtainTypedArray(R.array.items_images);

        float layoutweight = 1;

        //set linear layout 1 & 2 params
        LinearLayout layout1 = new LinearLayout(MainActivity.this);
        layout1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,layoutweight));

        layout1.setOrientation(LinearLayout.HORIZONTAL);
        layout1.setId(View.generateViewId());
        layout1.setGravity(Gravity.CENTER_HORIZONTAL);


        LinearLayout layout2 = new LinearLayout(MainActivity.this);
        layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,layoutweight));
        layout2.setOrientation(LinearLayout.HORIZONTAL);
        layout2.setId(View.generateViewId());
        layout2.setGravity(Gravity.CENTER_HORIZONTAL);

        for(int i = 0; i < 10 ; i++){
            if(i<5){
                layout1.addView(iv[i]);
            } else {
                layout2.addView(iv[i]);
            }
            iv[i].getLayoutParams().height=140;
            iv[i].getLayoutParams().width=140;
            iv[i].requestLayout();

            iv[i].setTag(i);
            iv[i].setOnClickListener(listener);
        }


        constLayout.addView(layout1,0);
        constLayout.addView(layout2,0);

        ConstraintSet set = new ConstraintSet();

        set.clone(constLayout);

        //set dimensions for margins
        int topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,40,getResources().getDisplayMetrics());
        int topMargin2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,10,getResources().getDisplayMetrics());
        int leftMargin=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,0,getResources().getDisplayMetrics());
        int rightMargin=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,0,getResources().getDisplayMetrics());


        //set constraints
        set.connect(layout1.getId(),ConstraintSet.TOP,R.id.constraint1,ConstraintSet.TOP,topMargin);
        set.connect(layout2.getId(),ConstraintSet.TOP,layout1.getId(),ConstraintSet.BOTTOM,topMargin2);
        set.connect(layout1.getId(),ConstraintSet.LEFT,R.id.constraint1,ConstraintSet.LEFT,leftMargin);
        set.connect(layout1.getId(),ConstraintSet.RIGHT,R.id.constraint1,ConstraintSet.RIGHT,rightMargin);
        set.connect(layout2.getId(),ConstraintSet.LEFT,R.id.constraint1,ConstraintSet.LEFT,leftMargin);
        set.connect(layout2.getId(),ConstraintSet.RIGHT,R.id.constraint1,ConstraintSet.RIGHT,rightMargin);

        set.centerHorizontally(layout1.getId(),R.id.constraint1);
        set.centerHorizontally(layout2.getId(),R.id.constraint1);

        set.applyTo(constLayout);

        pgbar = findViewById(R.id.progressBar);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Choose a Topping")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if(counter < 10){
                            selections[counter] = items[i];

                            iv[counter].setImageResource(img.getResourceId(i, -1));

                            //updates progress bar
                            if(counter == 0){
                                pgbar.setProgress(10);
                            }else{
                                pgbar.setProgress((counter+1)*10);
                            }
                            counter++;
                        }else{
                            Toast.makeText(getApplicationContext(),"Maximum toppings capacity reached!",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        final AlertDialog options = builder.create();

        findViewById(R.id.btnTopping).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                options.show();
            }
        });

        // Intent to send values to CheckoutDetails.java
        btnCalculateTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                toppingPrize = (counter * 1.50);

                if(deliveryCheckBox.isChecked())
                {
                    delivery=true;
                }
                else
                {
                    delivery=false;
                }

                Intent intent = new Intent(MainActivity.this, CheckoutDetails.class);
                Bundle bundle = new Bundle();
                bundle.putDouble("basePrize",basePrize);
                bundle.putDouble("toppingPrize", toppingPrize);
                bundle.putBoolean("deliveryCharge",delivery);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();

            }
        });

        //clear pizza
        findViewById(R.id.btnClear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for(int i = 0; i < 10 ; i++){
                    iv[i].setImageDrawable(null);
                }

                Arrays.fill(selections,null);

                counter=0;

                pgbar.setProgress(0);

            }
        });

        iv_item1.setTag(0);
        iv_item1.setOnClickListener(listener);

    }
}