/*
Assignment - Homework 02
File - CheckoutDetails.java
Name - Priya Patel & Pallav Jhaveri
 */

package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import static com.example.myapplication.MainActivity.selections;


public class CheckoutDetails extends AppCompatActivity {
    TextView baseCharge;
    TextView toppingCharge;
    TextView deliveryCharge;
    TextView arrayTextView;
    TextView totalCharge;
    Double total_cost = 0.0;
    String toppings = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_details);


        Bundle bundle = getIntent().getExtras();
        Double base = bundle.getDouble("basePrize");
        Double topping = bundle.getDouble("toppingPrize");
        Boolean delivery = bundle.getBoolean("deliveryCharge");

        baseCharge = findViewById(R.id.basePrice);
        toppingCharge=findViewById(R.id.toppingCharge);
        deliveryCharge = findViewById(R.id.deliveryCharge);
        arrayTextView = findViewById(R.id.arrayTextView);
        totalCharge = findViewById(R.id.totalCharge);

        for(int i = 0; i < selections.length; i++){
            if(selections[i] != null){
                if(i > 0){
                    toppings = toppings + " , " + selections[i];
                }else {
                    toppings = toppings + selections[i];
                }
            }
        }

        if(delivery == true){
            total_cost = base + 4.0 + topping;
            deliveryCharge.setText("4.0");

        }else {
            total_cost = base + topping;
            deliveryCharge.setText("0.0");
        }

        baseCharge.setText(base.toString());
        toppingCharge.setText(topping.toString());
        totalCharge.setText(total_cost.toString());
        arrayTextView.setText(toppings);

        findViewById(R.id.btnFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CheckoutDetails.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
